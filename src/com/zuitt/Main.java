package com.zuitt;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scannerName = new Scanner(System.in);
        System.out.println("First Name:");

        String firstName = scannerName.nextLine();
        System.out.println(firstName);

        System.out.println("Last Name:");

        String lastName = scannerName.nextLine();
        System.out.println(lastName);

        System.out.println("First Subject Grade:");
        double first = scannerName.nextDouble();
        System.out.println(first);

        System.out.println("Second Subject Grade:");
        double second = scannerName.nextDouble();
        System.out.println(second);

        System.out.println("Third Subject Grade:");
        double third = scannerName.nextDouble();
        System.out.println(third);

        double average = (first + second + third)/3;

        System.out.println("Your average is:");
        int value = (int)average;
        System.out.println(value);



    }
}
